# TODO APP

## Run application

Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`.

## Time spend on application development

- Creating a new project with git repository:   **less than 1h**
- Application styling:                          **+- 2h**
- Implementation of all functions:              **+- 4h**
- Wandering in the mind                         **+- 1h**

**summary spend time: +- 8h**
