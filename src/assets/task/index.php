<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="src/assets/app.css"/>
</head>
<body>

<!-- Your code here -->
<div class="todo__container">
    <div class="todo__add">
        <input type="text" class="todo__input" />
    </div>

    <ul class="todo__list">
        <li>
            <label>
                <input type="checkbox" />
                <input type="text" value="Post a new shot to Dribble" />
            </label>
        </li>
        <li>
            <label>
                <input type="checkbox" />
                <input type="text" value="Give away some PSDs" />
            </label>
        </li>
        <li>
            <label>
                <input type="checkbox" />
                <input type="text" value="Finish client work" />
            </label>
        </li>
    </ul>
</div>

</body>
</html>
