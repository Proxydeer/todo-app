import {Component, Input, OnInit} from '@angular/core';
import {Todo} from '../../shared/models/todo';
import {ToDoService} from '../../shared/services/to-do.service';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.scss']
})
export class ToDoComponent implements OnInit {
  @Input() todo: Todo;
  private todoCopy = new Todo();

  constructor(private toDoService: ToDoService) {
  }

  ngOnInit() {
    this.prepareCopyTask();
  }

  updateTask(todo, keypress = null) {
    if (!keypress || keypress.key === 'Enter') {
      if (this.todoCopy.task !== todo.task || this.todoCopy.is_completed !== todo.is_completed) {
        this.toDoService.updateTask(todo);
        this.prepareCopyTask();
      }
    }
  }

  changeCheckbox(todo: Todo) {
    todo.is_completed === 1 ? todo.is_completed = 0 : todo.is_completed = 1;
  }

  deleteTask(id: string) {
    this.toDoService.deleteTask(id);
  }

  private prepareCopyTask() {
    this.todoCopy.task = this.todo.task;
    this.todoCopy.is_completed = this.todo.is_completed;
  }

}
