import {Component, OnInit} from '@angular/core';
import {Todo} from '../shared/models/todo';
import {ApiService} from '../shared/services/api.service';
import {map, share, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {TodoListDto} from '../shared/models/todoListDto';
import {ToDoService} from '../shared/services/to-do.service';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.scss'],
  providers: [ToDoService]
})
export class ToDoListComponent implements OnInit {
  newTask = new Todo();

  constructor(public toDoService: ToDoService) {
  }

  ngOnInit() {
  }

  addNewTask(keypress = null) {
    if (!keypress || keypress.key === 'Enter') {
      if (this.newTask.task && this.newTask.task.length >= 1) {
        this.toDoService.addTask(this.newTask);
        this.newTask = new Todo();
      }
    }
  }

  changeCheckbox(todo: Todo) {
    todo.is_completed === 1 ? todo.is_completed = 0 : todo.is_completed = 1;
  }
}
