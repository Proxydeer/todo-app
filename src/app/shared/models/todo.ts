export class Todo {
  id: string;
  candidate: string;
  task: string;
  is_completed = 0;
}
