import {Todo} from "./todo";

export class TodoListDto {
  status: string;
  data: Todo[] = [];
}
