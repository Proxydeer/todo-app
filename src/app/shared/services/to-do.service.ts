import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {ApiService} from './api.service';
import {Todo} from '../models/todo';

@Injectable({
  providedIn: 'root'
})
export class ToDoService {
  toDoList$ = new BehaviorSubject<Todo[]>([]);

  constructor(private apiService: ApiService) {
    this.apiService.getAllTasks().subscribe((response) => {
      this.toDoList$.next(response);
    });
  }

  updateTask(todo: Todo) {
    this.apiService.updateOrAddTask(todo).subscribe((updatedTask: Todo) => {
      const list = this.toDoList$.getValue();
      list.forEach((task, index) => {
        if (task.id === updatedTask.id) {
          task = updatedTask;
        }
      });
      this.toDoList$.next(list);
    });
  }

  addTask(todo: Todo) {
    this.apiService.updateOrAddTask(todo).subscribe((newTask: Todo) => {
      const list = this.toDoList$.getValue();
      list.unshift(newTask);
      this.toDoList$.next(list);
    });
  }

  deleteTask(id: string) {
    this.apiService.deleteTask(id).subscribe((deletedTask: Todo) => {
      const list = this.toDoList$.getValue();
      list.forEach((task, index) => {
        if (task.id === deletedTask.id) {
          list.splice(index, 1);
        }
      });
      this.toDoList$.next(list);
    });
  }

}
