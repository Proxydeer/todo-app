import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TodoListDto} from '../models/todoListDto';
import {Todo} from '../models/todo';
import {filter, map, tap} from 'rxjs/operators';

@Injectable()
export class ApiService {
  apiPrefix = 'http://tests.mpcreation.net/api/hachula.daniel';

  constructor(private http: HttpClient) {
  }

  getAllTasks(): Observable<Todo[]> {
    return this.http.get<TodoListDto>(this.apiPrefix).pipe(
      filter((res) => res.status === 'success'),
      map((toDoListDto: TodoListDto) => toDoListDto.data),
      tap((list) => console.log('Init todo list: ', list))
    );
  }

  updateOrAddTask(todo: Todo): Observable<Todo> {
    return this.http.post<TodoListDto>(this.apiPrefix, this.convertObjectToFormData(todo)).pipe(
      filter((res) => res.status === 'success'),
      map((toDoListDto: TodoListDto) => toDoListDto.data[0]),
      tap((task) => console.log('Task added or updated: ', task))
    );
  }

  deleteTask(id: string): Observable<Todo> {
    return this.http.delete(this.apiPrefix + '/' + id).pipe(
      filter((res: TodoListDto) => res.status === 'success'),
      map((toDoListDto: TodoListDto) => {
        if (Array.isArray(toDoListDto.data)) {
          return toDoListDto.data[0];
        } else {
          return toDoListDto.data;
        }
      }),
      tap((task) => console.log('Task deleted: ', task))
    );
  }

  private convertObjectToFormData(object: Todo): FormData {
    const formData = new FormData();
    if (object) {
      // @ts-ignore
      formData.append('is_completed', object.is_completed);
      formData.append('title', object.task);
      if (object.id) {
        formData.append('id', object.id);
      }
    }
    return formData;
  }

}


